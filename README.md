# ipynb2pdf

## C'est quoi ça ?

Script bash permettant d'exporter un notebook au format ipynb en PDF.

## Description

Pour imprimer les notebooks de Capytale, il faut les exporter en PDF. Ce script permet de conserver (un peu) la mise en page du notebook en permettant de préserver les **admonitions**.


## Captures d'écran

![](./test/algorithmes-gloutons_preview.png)

![](./test/nsi_preview.png)


## Installation

### Dépendances
 - xelatex (pour éviter les erreurs de UTF8 en latex)
 - sed (expressions régulières)
 - nbconvert (et donc jupyter) (pour ipynb -> md)
 - pandoc (pour md -> pdf)
 - le template eisvogel.latex (c'est beau)
 - le filtre pandoc-latex-environment (c'est beau)


### Exemples d'installations (Debian)

- INSTALLATION xelatex
    - `$ sudo apt install texlive-xetex`
- INSTALLATION nbconvert :
    - `$ sudo apt install jupyter-nbconvert`
- INSTALLATION template Eisvogel
    - `$ wget https://github.com/Wandmalfarbe/pandoc-latex-template/releases/download/2.4.2/Eisvogel.zip`
    - `$ cp eisvogel.latex ~/.local/share/pandoc/templates/` (créer les dossiers si besoin)
- INSTALLATION filtre pandoc-latex-environment
    - `$ pip install pandoc-latex-environment --break-system-packages`
- INSTALLATION polices supplémentaires (manquait chez moi)
    - `$ sudo apt install texlive-fonts-extra`



## Usage

`ipynb2pdf NOTEBOOK CONFIGURATION";`

avec 
- le fichier NOTEBOOK (au format ipynb) ;
- le fichier CONFIGURATION (au formal yaml) qui permet de paramétrer l'export.

Le fichier CONFIGURATION est un header ajouté pour paramétrer pandoc et qui permet de bien exporter les admonitions. Voir dans le dossier `test` pour avoir des exemples de fichiers corrects.

Par défaut l'export se fait au format A5.


## Roadmap

- faire en sorte que les noms de fichier **avec** des espaces fonctionnent (dommage...)
- mettre de meilleurs fichiers de tests/exemples 
    - illustrations libres
    - exemples parlant
    - productions persos
- remplacer le fichier de configuration par des paramètres de script



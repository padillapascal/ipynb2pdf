#!/bin/bash
#
# convertions de notebook issus de capytale (avec admonitions)
# vers du PDF
# 
# 
# Dépendances :
# - xelatex (pour éviter les erreurs de UTF8 en latex)
# - sed (expressions régulières)
# - nbconvert (et donc jupyter) (pour ipynb -> md)
# - pandoc (pour md -> pdf)
# - le template eisvogel.latex (c'est beau)
# - le filtre pandoc-latex-environment (c'est beau)
#
#
# Exemples d'installations sur Debian
#
# INSTALLATION xelatex
# $ sudo apt install texlive-xetex
#
# INSTALLATION nbconvert :
# $ sudo apt install jupyter-nbconvert
#
# INSTALLATION template eisvogel
# $ wget https://github.com/Wandmalfarbe/pandoc-latex-template/releases/download/2.4.2/Eisvogel.zip
# $ cp eisvogel.latex ~/.local/share/pandoc/templates/
# (créer les dossiers si besoin)
#
# INSTALLATION filtre pandoc-latex-environment
# $ pip install pandoc-latex-environment --break-system-packages
#
# INSTALLATION polices supplémentaires (manquait chez moi)
# $ sudo apt install texlive-fonts-extra



# documentation
usage() {
    echo "Usage: ipynb2pdf NOTEBOOK CONFIGURATION";
    echo "Exporte en PDF le fichier NOTEBOOK (au format ipynb).";
    echo "Le fichier CONFIGURATION (au formal yaml) permet de paramétrer l'export. C'est un header pour Pandoc." 1>&2;
    exit 1;
}


# erreur
if [ "$#" -ne 2 ]; then
    echo "ipynb2pdf: erreur il faut 2 arguments"
    usage
fi


# script
file="$1"
echo $file
jupyter-nbconvert --to markdown "$file" --output /tmp/__ipynb2pdf-p1
sed -r 's/!!! ([a-z]* )/::: \1\n/;s/\?\?\? ([a-z]* )/::: \1\n/;s/!!!/:::/;s/\?\?\?/:::/' /tmp/__ipynb2pdf-p1.md > /tmp/__ipynb2pdf-p2.md
sed h $2 /tmp/__ipynb2pdf-p2.md > /tmp/__ipynb2pdf-p3.md
pandoc /tmp/__ipynb2pdf-p3.md -o "$file.pdf" --pdf-engine=xelatex --from markdown --template eisvogel.latex --filter pandoc-latex-environment --listings
